###########################################
### Tools for Working With UMass Term Codes
### Jasper McChesney <jmcchesney@cs.umass.edu>
### v1.0 Aug 2020

# Works with UMA codes, ala 1187 (the term code) for Fall 2018 (the term name).
# All functions are vectorized. There is some input checking, but it is not robust.

is_term_code <- function(x) {
  # Checks for whether items in vecgtor x are valid 4-digit UMA term

  check <- nchar(as.character(x)) == 4 &
    substr(as.character(x), 1, 1) %in% c('0', '1') &
    substr(as.character(x), 4, 4) %in% c('1', '3', '5', '7')
  check[is.na(x)] <- NA
  return(check)
}

sem_code_to_name <- function(x) {
  # Converts the trailing digit of a term code to a semester name

  ifelse(x == 1, 'Winter',
  ifelse(x == 3, 'Spring',
  ifelse(x == 5, 'Summer',
  ifelse(x == 7, 'Fall', NA)))) }

sem_name_to_code <- function(x) {
  # Converts the name of a semester to the trailing digit of the term code

  ifelse(x == 'W' | x == 'Winter', 1,
  ifelse(x == 'S' | x == 'Spring', 3,
  ifelse(x == 'Su' | x == 'Summer', 5,
  ifelse(x == 'F' | x == 'Fall', 7,
  NA))))
}

term_code_to_semester <- function(x) {
  # Extracts the name of the semester in term codes
  sem_code_to_name(as.numeric(x) %% 10)
}

term_code_to_year <- function(x) {
  # Extracts the 4-digit (calendar) year from a term code

  if(!is.numeric(x)) { x <- as.numeric(as.character(x)) }
  1900 + 100 * (floor(x/1000)) + trunc((x %% 1000) / 10, 2) 
}

term_code_to_acyear <- function(x, output='fall') {
  # Extracts the 4-digit academic year from a term code
  # Normally returns the year in which Fall occurs, but change this with "output"

  if(!is.numeric(x)) { x <- as.numeric(as.character(x)) }
  aterm <- x - (10 * as.numeric(x %% 10 < 5))
  year1 <- term_code_to_year(aterm)
  
  if(output=='fall') return(year1)
  if(output=='spring') return(year1 + 1)
  if(output=='abbr') return(paste(substr(year1, 3, 5), substr(year1, 8, 9), sep=''))
  return(paste(year1, year1 + 1, sep='-'))
}

yearsem_to_term_code <- function(year, semester) {
  # Combines a vector of calendar years and a vector of semester names into term codes

  year <- as.numeric(year)
  sem_code <- if(all(is.numeric(semester))) semester else sem_name_to_code(semester)
    
  code <- paste(
    trunc(year / 1000) - 1, # century
    substr(as.character(year), 3, 4),  # decade
    sem_code, # semester
    sep = '')
  
  code[is.na(year)] <- NA
  code[is.na(semester)] <- NA
  return(as.numeric(code))
}

term_name_to_term_code <- function(name) {
  # Parses a term name such as "Fall 2009" and converts to code "1097" 
  
  semester <- tstrsplit(name, split=' ', keep=1)[[1]]
  year <- tstrsplit(name, split=' ', keep=2)[[1]]
  code <- yearsem_to_term_code(year=year, semester=semester)
  return(code)
}

diff_terms <- function(x, y) {
  # Computes a numeric interval between term codes, where one full semester = 1,
  # winter/summer = .5, and a full year = 2.

  if(!(all(is_term_code(x), na.rm=TRUE) & all(is_term_code(y), na.rm=TRUE))) stop('x and y must be 4-digit UMA term codes')
  if(is.factor(x) | is.character(x)) x <- as.numeric(as.character(x))
  if(is.factor(y) | is.character(y)) y <- as.numeric(as.character(y))
  
  x2 <- (x %/% 10) * 10 + ((x %% 10 - 1) * 1.25)
  y2 <- (y %/% 10) * 10 + ((y %% 10 - 1) * 1.25)
  d <- (x2 - y2) / 5
  return(d)
}

get_nterms <- function(current, admit, bump = TRUE) {
  # A wrapper around diff_terms() for the common case of computing what term a student is in since matriculating
  # The term of matriculation counts as 1 (not 0).
  # "bump = TRUE" considers a summer admit as fall, and winter as spring
  
  if(bump==TRUE) {
    tobump <- term_code_to_semester(admit) %in% c('Summer', 'Winter')
    admit[tobump] <- adj_term(admit[tobump], .5)
  }
  diff_terms(x = current, y = admit) + 1
}

adj_term <- function(x, adj) {
  # Takes a term code and adjusts by (signed) number of full semesters
  # where fall-spring = 1, to winter/summer = .5, and one year = 2
  
  if(! all(is_term_code(x), na.rm=TRUE)) stop('x must be 4-digit UMA term codes')
  if(is.character(x) | is.factor(x)) x <- as.numeric(as.character(x))
  if( all(adj %% .5 != 0, na.rm=T) ) stop('y must be a multiple of .5')

  # Convert to alternate term coding to allow arithmetic
  xsem <- x %% 10
  sem2 <- xsem + (xsem - 3) / 4
  x2 <- (x %/% 10) * 10 + sem2

  # perform operation
  x3 <- x2 + (adj * 5)
  
  # convert back to UMA code system
  x4 <- (x3 %/% 10) * 10 + (x3 %% 10) - ((x3 %% 10) -3) / 5
  
  return(x4)
}

date_to_term <- function(x) {
  # Converts from a date-time object to term codes

  #!is.Date(x) stop("x is not a datetime object.")

  xmo <- month(x)
  xday <- mday(x)
  
  sem <- rep(NA, length(x))
  sem[xmo >= 9 & xmo <= 12] <- 7 # encode Sep-Dec as Fall
  sem[xmo == 1] <- 1 # encode Jan as Winter
  sem[xmo >= 2 & (xmo < 5 | (xmo == 5 & xday <= 15))] <- 3 # encode Feb to Mid-May as Spring
  sem[((xmo == 5 & xday > 15) | xmo > 5) & xmo < 9] <- 5 # encode Mid-May to Aug as Summer
  
  yr <- as.character(year(x) %% 100)
  yr[nafalse(nchar(yr) == 1)] <- paste('0', yr[nafalse(nchar(yr) == 1)], sep='')
  
  mil <- as.character(year(x) %/% 1000 - 1)
  
  term <- paste(mil, yr, sem, sep='')
  term[is.na(x)] <- NA
  return(term)
}

term_code_to_date <- function(x, fall_mo=9, spring_mo=2, winter_mo=1, summer_mo=5) {
  # Convert term code to date-time format
  
  if(!is_term_code(x)) stop('x is not a valid UMA term code')
  
  year <- term_code_to_year(x)
  sem <- term_code_to_semester(x)
  
  mo <- rep(NA, length(x))
  mo[sem=='Fall'] <- fall_mo
  mo[sem=='Spring'] <- spring_mo
  mo[sem=='Summer'] <- summer_mo
  mo[sem=='Winter'] <- winter_mo
  
  date <- as.Date(paste(year, mo, 01, sep='-'))
  return(date)
}
